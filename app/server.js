const express = require('express');
const { Pool } = require('pg');

const app = express();
const port = process.env.PORT || 3000;

const pool = new Pool({
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
});

app.get('/', async (req, res) => {
  try {
    const client = await pool.connect();
    const result = await client.query('SELECT NOW()');
    const currentTime = result.rows[0].now;
    client.release();

    res.send(`Hello, the current time is ${currentTime}.`);
  } catch (err) {
    console.error(err);
    res.send('Error occurred while connecting to the database.');
  }
});

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});
